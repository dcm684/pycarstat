# Acquires data from a vehicle's OBD II port and can record and or  
# display the data in a user configurable manner. Such display methods
# include graphs, gauges, and text.

import wx
import OCS_Graph
import OCS_Widgets
import OCS_DataInterface

class ProgramFrame(wx.Frame):
    """
    The top-level frame for the program
    """
    ID_EXIT = 199
    ID_ABOUT = 901
    
    title = 'osCarStat OBD II Interface'
    
    def __init__(self):
        """
        Initializes the layout of the main program frame
        """
        
        wx.Frame.__init__(self, None, -1, self.title)
        self.CreateStatusBar()
        self.SetStatusText("")
        
        fileMenu = wx.Menu()
        fileMenu.Append(self.ID_EXIT, "E&xit", "Exit the program")
        
        helpMenu = wx.Menu()
        helpMenu.Append(self.ID_ABOUT, "&About", "About osCarStat")
        
        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, "&File")
        menuBar.Append(helpMenu, "&Help")
        
        self.SetMenuBar(menuBar)
        
        wx.EVT_MENU(self, self.ID_EXIT, self.exit_program)
        wx.EVT_MENU(self, self.ID_ABOUT, self.about_prompt)
        
        #self.SetSize((600,600))
        
        self.obdInterface = \
            OCS_DataInterface.OCS_Interface(OCS_DataInterface.CONN_RANDOM, 0, 0)
        
        self.widgetContainer = OCS_Widgets.DataPanelContainer(self)        
        self.widgetContainer.add_panel(0x10, OCS_Widgets.PANEL_LINE_GRAPH)
        self.widgetContainer.add_panel(0x0A, OCS_Widgets.PANEL_LINE_GRAPH)
        self.widgetContainer.add_panel(0x0D, OCS_Widgets.PANEL_LINE_GRAPH)
        
        self.updateCounter = 0
        
        self.Layout()
        
        self.checkPeriod = 500
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_redraw_timer, self.redraw_timer)        
        self.redraw_timer.Start(self.checkPeriod)
        
    def exit_program(self, event):
        """
        Handles the closing of the program
        """
        
        self.Close(True)
        
    def about_prompt(self, event):
        """
        Displays a box showing the version and copyright information
        for the program
        """ 
        
        dlg = wx.MessageDialog(self, "osCarStat\n\n"
                                "Version 0.01\n"
                                "Copyright 2011\n\n"
                                "Developed by:\n"
                                "Christopher Meyer",
                                "About osCarStat",
                                wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()
        
    def on_redraw_timer(self, event):
        """
        Everytime the update timer occurs, the all of the widgets are
        updated and the timer increments
        """
        
        self.widgetContainer.update_all_widgets()

        self.updateCounter = self.updateCounter + 1
        
    def get_time(self):
        """
        Returns a float of the current run time
        """
        return self.updateCounter * float(self.checkPeriod) / 1000
    
    def get_pid(self, pid):
        #Holder
        return self.obdInterface.request_data(pid)
        
    def set_check_freq(self, msCheckPeriod):
        """Sets the time between PID checks in ms"""
        self.checkPeriod = msCheckPeriod
        

if __name__ == '__main__':
    app = wx.PySimpleApp()
    app.frame = ProgramFrame()
    app.frame.Show()
    app.MainLoop()