import serial
import random

SEND_PID_REQ = 'R'

CONN_RANDOM = 0
CONN_MCP2515 = 1
CON_ELM327 = 2

PID_NON_OBD = 0xF0
PID_FUEL_ECON = 0xF0
PID_ACCEL_X = 0xF1
PID_ACCEL_Y = 0xF2
PID_ACCEL_Z = 0xF3

ACCEL_AXIS_X = 0x01
ACCEL_AXIS_Y = 0x02
ACCEL_AXIS_Z = 0x04

INTERFACE_ACCEL_LIST = 'l'
INTERFACE_ACCEL_CAL_ALL = 'C'

def get_available_ports():
    availablePorts = []
    return availablePorts

class OCS_InvalidInterfaceException(Exception): pass

class OCS_Interface():
    def __init__(self, type, port, rate = 500000):
        """
        Creates an interface between the computer and the OBD-II device
        
        Params
        type -- Type of connection used. Possible values
                CONN_RANDOM -- Random values
                CON_MCP2515 -- Microchip MCP2515 CAN SPI
                CON_ELM327 -- ELM 327 chip
        port -- The name of the port to connect to
        rate -- The data rate of the connection
        """
        if type == CONN_RANDOM:
            self.interface = OCS_RandomDataInterface()
        elif type == CONN_MCP2515:
            self.interface = OCS_MCP2515Interface()
        else:
            raise OCS_InvalidInterfaceException()
        
    def request_data(self, pid):
        #Calculate fuel economy
        if pid == 0xF0:
            return self.calcFuelEcon()
        return self.interface.request_data(pid)
        
    def requestAccel(self):
        """
        Returns the accelerometers that are present and if there are
        accelerometers that present, they will be calibrated
        
        Returns:
        Bit-encoded list of accelerometers that are present
            ACCEL_AXIS_X
            ACCEL_AXIS_Y
            ACCEL_AXIS_Z
        """
        
        
    def calcFuelEcon(self):
        """
        Returns the fuel economy in L / 100km
        
        MPG = (14.7 * 6.17 * 454 * VSS * 0.621371) / (3600 * MAF / 100) (E0 Gas)
        MPG = 710.7 * VSS / MAF (With E0 Gasoline)
         
        MPG = (14.13 * 6.211 * 454 * VSS * 0.621371) / (3600 * MAF / 100) (E10)
        MPG = 687.7 * VSS / MAF (With E10 Gasoline)
         
        MPG - miles per gallon
        
        LPK = (3600 * MAF / 100) / (14.7 * 730 * VSS) (E0 Gas)
        LPK = .0033547 * MAF / VSS
        LPK = (3600 * MAF / 100) / (14.7 * 734.85 * VSS) (E10 Gas)
        LPK = .0033326 * MAF / VSS
        
        LPK = Liters per KM
        
        14.7 grams of air to 1 gram of gasoline - ideal air/fuel ratio
        6.17 pounds per gallon - density of gasoline (730 grams per Liter)
         
        14.13 grams of air to 1 gram of E10
        6.58 pounds per gallon for E100
        6.211 pounds per gallon E10 (734.85 grams per liter)
         
        454 grams per pound - conversion
        VSS - vehicle speed in kilometers per hour
        0.621371 miles per hour/kilometers per hour - conversion
        3600 seconds per hour - conversion
        MAF - mass air flow rate in 100 grams per second
        100 - to correct MAF to give grams per second
         
        """
        vss = self.interface.request_data(0x0D)
        if vss > 0:
            maf = self.interface.request_data(0x10)
            return 0.0033547 * maf / vss * 100
        else:
            return 0
        
    


class OCS_RandomDataInterface():
    def __init__(self):
        return
    
    def request_data(self, pid):
        retVal = OCS_PID_Translate(pid, random.randint(0, 2**(8 * PID_TABLE[pid][1])))
        return retVal
    
    def requestAccel(self):
        return ACCEL_AXIS_X | ACCEL_AXIS_Y | ACCEL_AXIS_Z

class OCS_MCP2515Interface ():
    def __init__(self, port, rate = 500000):
        """
        Initializes an interface with the OBDII bus via the given serial
        port at the given speed.
        
        port -- The name of the port to connect to
        rate -- The data rate of the connection
        """  
        
        #Try to open the given port at the given data rate
        try:
            self.mySerial = serial.Serial(port, rate, timeout=1)
        except serial.SerialException:
            print 'Could not connect to ' + port
            raise serial.SerialException
            

    
    def request_data(self, pid):
        """
        Sends a request to the serial interface for data for a specific
        PID. The value received is returned.
        
        If the receive times out False will be returned.
        
        PIDs less or equal to than 0xFF are assumed to be OBDII PIDs. 
        Those greater are for non-OBDII values.
        
        pid -- An integer OBDII PID that is to be requested.
        
        Returns -- Integer value of data received 
        
        """
        
        try:
            self.mySerial.write(SEND_PID_REQUEST)
            self.mySerial.write(pid)
            incomingBytes = self.mySerial.read(1)
            inData = self.mySerial.read(incomingBytes)
        except serial.SerialTimeoutException:
            print 'Timed out requesting ' + hex(pid)
            inData = 0
            
        return int(inData)
        
    def requestAccel(self):
        """
        Requests a list of available accelerometer axes and calibrates them
        
        Returns:
        Bit-encoded list of accelerometers that are present
            ACCEL_AXIS_X
            ACCEL_AXIS_Y
            ACCEL_AXIS_Z
        """
        try:
            self.mySerial.write(INTERFACE_ACCEL_LIST)
            axes = self.mySerial.read(1)
            self.mySerial.write(INTERFACE_ACCEL_CAL_ALL)
            self.mySerial.read(1)
        except serial.SerialTimeoutException:
            print 'Timed out requesting ' + hex(pid)
            axes = 0
            
        return axes
    
UNIT_BITENC = 253
UNIT_MIXED = 254
UNIT_NONE = 0
UNIT_PERCENT = 1
UNIT_PA = 2
UNIT_KPA = 3
UNIT_KMH = 4
UNIT_GS = 5
UNIT_VOLT = 6
UNIT_C = 7
UNIT_SEC = 8
UNIT_MIN = 9
UNIT_NM = 10
UNIT_LH = 11
UNIT_DEGREE = 12
UNIT_KM = 13

UNIT_METRIC = 0
UNIT_US = 1

UNIT_TEXT = [[
                "",
                "%",
                "Pa",
                "kPa",
                "km/h",
                "g/s",
                "V",
                unichr(176) + "C",
                "s",
                "min",
                "Nm",
                "L/h",
                unichr(176),
                "km"
                ], [
                "",
                "%",
                "psi",
                "psi",
                "mph",
                "lb/min",
                "V",
                unichr(176) + "F",
                "s",
                "min",
                "ft" + unichr(183) + "lb",
                "L/h",
                unichr(176),
                "miles"
                ]
              ]



PID_TABLE = [
    ["PIDs supported [0x01 - 0x20]", 4, UNIT_BITENC],
    ["Monitor status since DTCs cleared", 4, UNIT_BITENC],
    ["Freeze DTC", 2, UNIT_BITENC],
    ["Fuel System Status", 2, UNIT_BITENC],
    ["Calculated Engine Load", 1, UNIT_PERCENT],
    ["Engine Coolant Temperature", 1, UNIT_C],
    ["Bank 1 Short Term Fuel % Trim", 1, UNIT_PERCENT],
    ["Bank 1 Long Term Fuel % Trim", 1, UNIT_PERCENT],
    ["Bank 2 Short Term Fuel % Trim", 1, UNIT_PERCENT],
    ["Bank 2 Long Term Fuel % Trim", 1, UNIT_PERCENT],
    ["Fuel Pressure (Gauge)", 1, UNIT_KPA],
    ["Intake Manifold Absolute Pressure (Absolute)", 1, UNIT_KPA],
    ["Engine RPM", 2, UNIT_NONE],
    ["Vehicle Speed", 1, UNIT_KMH],
    ["Timing Advance, w.r.t. Cylinder 1", 1, UNIT_DEGREE],
    ["Intake Air Temperature", 1, UNIT_C],
    ["Mass Air Flow Rate", 2, UNIT_GS],
    ["Throttle Position", 1, UNIT_PERCENT],
    ["Commanded Secondary Air Status", 1, UNIT_BITENC],
    ["Oxygen Sensors Present", 1, UNIT_BITENC],
    ["Oxygen Sensor Data, Bank 1 Sensor 1", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 1 Sensor 2", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 1 Sensor 3", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 1 Sensor 4", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 2 Sensor 1", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 2 Sensor 2", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 2 Sensor 3", 2, UNIT_MIXED],
    ["Oxygen Sensor Data, Bank 2 Sensor 4", 2, UNIT_MIXED],
    ["Vehicle ODB Standard", 1, UNIT_BITENC],
    ["Oxygen Sensors Present", 1, UNIT_BITENC],
    ["Auxiliary Input Status", 1, UNIT_BITENC],
    ["Run Time Since Engine Start", 2, UNIT_SEC],
    ["PIDs supported [0x21 - 0x40]", 4, UNIT_BITENC],
    ["Distance Traveled with the MIL On", 2, UNIT_KM],
    ["Fuel Rail Pressure Relative to Manifold Vacuum", 2, UNIT_KPA],
    ["Fuel Rail Pressure (Diesel)", 2, UNIT_KPA],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 1 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 2 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 3 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 4 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 1 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 2 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 3 (Voltage)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 4 (Voltage)", 4, UNIT_MIXED],
    ["Commanded EGR", 1, UNIT_PERCENT],
    ["EGR Error", 1, UNIT_PERCENT],
    ["Commanded Evaporative Purge", 1, UNIT_PERCENT],
    ["Fuel Level Input", 1, UNIT_PERCENT],
    ["Number of Warm-Ups Since Codes Cleared", 1, UNIT_NONE],
    ["Distance Traveled Since Codes Cleared", 2, UNIT_NONE],
    ["Evaporative System Pressure", 2, UNIT_PA],
    ["Barometric Pressure (Absolute)", 1, UNIT_KPA],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 1 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 2 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 3 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 1 Sensor 4 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 1 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 2 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 3 (Current)", 4, UNIT_MIXED],
    ["Oxygen Sensor WR Lambda, Bank 2 Sensor 4 (Current)", 4, UNIT_MIXED],
    ["Catalyst Temperature Bank 1 Sensor 1", 2, UNIT_C],
    ["Catalyst Temperature Bank 1 Sensor 2", 2, UNIT_C],
    ["Catalyst Temperature Bank 2 Sensor 1", 2, UNIT_C],
    ["Catalyst Temperature Bank 2 Sensor 2", 2, UNIT_C],
    ["PIDs Supported [0x41 - 0x60]", 4, UNIT_BITENC],
    ["Monitor Status This Drive Cycle", 4, UNIT_BITENC],
    ["Control Module Voltage", 2, UNIT_VOLT],
    ["Absolute Load Value", 2, UNIT_PERCENT],
    ["Common Equivalence Ratio", 2, UNIT_NONE],
    ["Relative Throttle Position", 1, UNIT_PERCENT],
    ["Ambient Air Temperature", 1, UNIT_C],
    ["Absolute Throttle Position B", 1, UNIT_PERCENT],
    ["Absolute Throttle Position C", 1, UNIT_PERCENT],
    ["Absolute Throttle Position D", 1, UNIT_PERCENT],
    ["Absolute Throttle Position E", 1, UNIT_PERCENT],
    ["Absolute Throttle Position F", 1, UNIT_PERCENT],
    ["Commanded Throttle Actuator", 1, UNIT_PERCENT],
    ["Time Run with MIL On", 2, UNIT_MIN],
    ["Time Since Trouble Codes Cleared", 2, UNIT_MIN],
    ["Maximum Values for Equiv Ratio, O2 Sensor Voltage and Current, and Intake Manifold Absolute Pressure", 4, UNIT_MIXED],
    ["Maximimum Value for Air Flow Rate from MAF", 4, UNIT_GS],
    ["Fuel Type", 1, UNIT_BITENC],
    ["Ethanol Fuel Content", 1, UNIT_PERCENT],
    ["Absolute Evaporative System Vapor Pressure", 2, UNIT_KPA],
    ["Evaporative System Pressure", 2, UNIT_PA],
    ["Secondary Oxygen Sensor Trim Short Term Bank 1 and 3", 2, UNIT_PERCENT],
    ["Secondary Oxygen Sensor Trim Long Term Bank 1 and 3", 2, UNIT_PERCENT],
    ["Secondary Oxygen Sensor Trim Short Term Bank 2 and 4", 2, UNIT_PERCENT],
    ["Secondary Oxygen Sensor Trim Long Term Bank 2 and 4", 2, UNIT_PERCENT],
    ["Fuel Rail Pressure", 2, UNIT_KPA],
    ["Relative Accelerator Pedal Position", 1, UNIT_PERCENT],
    ["Hybrid Battery Pack Remaining Life", 1, UNIT_PERCENT],
    ["Engine Oil Temperature", 1, UNIT_C],
    ["Fuel Injection Timing", 2, UNIT_DEGREE],
    ["Engine Fuel Rate", 2, UNIT_LH],
    ["Emission Requirements to Which the Vehicle is Designed", 1, UNIT_BITENC],
    ["PIDs supported [0x61 - 0x80]", 4, UNIT_BITENC],
    ["Driver's Demand Engine Percent Torque", 1, UNIT_PERCENT],
    ["Actual Engine Percent Torque", 1, UNIT_PERCENT],
    ["Engine Reference Torque", 2, UNIT_NM],
    ["Engine Percent Torque Data", 5, UNIT_PERCENT],
    ["Auxiliary Input / Output Supported", 2, UNIT_BITENC],
    ["Mass Air Flow Sensor", 5, UNIT_MIXED],
    ["Engine Coolant Temperature", 3, UNIT_MIXED],
    ["Intake Air Temperature Sensor", 7, UNIT_MIXED],
    ["Commanded EGR and EGR Error", 7, UNIT_MIXED],
    ["Commanded Diesel Intake Air Flow Control and Relative Intake Air Flow Position", 5, UNIT_MIXED],
    ["Exhaust Gas Recirculation Temperature", 5, UNIT_MIXED],
    ["Commanded Throttle Actuator Control and Relative Throttle Position", 5, UNIT_MIXED],
    ["Fuel Pressure Control System", 6, UNIT_MIXED],
    ["Injection Pressure Control System", 5, UNIT_MIXED],
    ["Turbocharger Compressor Inlet Pressure", 3, UNIT_MIXED],
    ["Boost Pressure Control", 9, UNIT_MIXED],
    ["Variable Geometry Turbo (VGT) Control", 5, UNIT_MIXED],
    ["Waste Gate Control", 5, UNIT_MIXED],
    ["Exhaust Pressure", 5, UNIT_MIXED],
    ["Turbocharger RPM", 5, UNIT_MIXED],
    ["Turbocharger A Temperature", 7, UNIT_MIXED],
    ["Turbocharger B Temperature", 7, UNIT_MIXED],
    ["Charge Air Cooler Temperature (CACT)", 5, UNIT_MIXED],
    ["Exhaust Gas Temperature (EGT) Bank 1", 9, UNIT_MIXED],
    ["Exhaust Gas Temperature (EGT) Bank 2", 9, UNIT_MIXED],
    ["Diesel Particulate Filter Bank 1", 7, UNIT_MIXED],
    ["Diesel Particulate Filter Bank 2", 7, UNIT_MIXED],
    ["Diesel Particulate Filter Temperature", 9, UNIT_MIXED],
    ["NOx NTE Control Area Status", 1, UNIT_BITENC],
    ["PM NTE Control Area Status", 1, UNIT_BITENC],
    ["Engine Run Time", 13, UNIT_MIXED]
]

TEST_NOT_SUPPORTED = 1 # Test unsupported
TEST_SUPPORTED_COMP = 2 # Test is supported and is completed
TEST_SUPPORTED_INCOMP = 3 # Test is supported but has not completed

M1P03_NONE = 0
M1P03_OPEN_TEMP = 1
M1P03_CLOSED_O2 = 2
M1P03_OPEN_LOAD = 3
M1P03_OPEN_FAULT = 4
M1P03_CLOSED_FAULT = 5

M1P12_NONE = 0
M1P12_UPSTREAM = 1
M1P12_DOWNSTREAM = 2
M1P12_OUTSIDE_OFF = 3

OBD_TYPE_ERROR = 0xFF # Emission Requirement Was Out of Range
OBD_TYPE_CARB = 1 # OBD-II as defined by the CARB 
OBD_TYPE_EPA = 2 # OBD as defined by the EPA
OBD_TYPE_12 = 3 # OBD and OBD-II
OBD_TYPE_1 = 4 # OBD-I
OBD_TYPE_NON = 5 # Not meant to comply with any OBD Standard 
OBD_TYPE_E = 6 # EOBD (Europe)
OBD_TYPE_E2 = 7 # EOBD (Europe) and OBD 
OBD_TYPE_E1 = 8 # EOBD (Europe) and OBD-II
OBD_TYPE_E12 = 9 # EOBD (Europe), OBD, and OBD-II
OBD_TYPE_J = 0xA # JOBD (Japan)
OBD_TYPE_J2 = 0xB # JOBD (Japan) and OBD-II
OBD_TYPE_JE = 0xC # JOBD (Japan) and EOBD (Europe)
OBD_TYPE_JE2 = 0xD # JOBD (Japan), EOBD (Europe), and OBD-II
OBD_TYPE_EURO_IV_B1 = 0x0E # Heavy Duty Vehicles (Euro IV) B1
OBD_TYPE_EURO_V_B2 = 0x0F # Heavy Duty Vehicles (Euro V) B2
OBD_TYPE_EURO_EEV_C = 0x10 # Heavy Duty Vehicles (Euro EEV) C
OBD_TYPE_EMD = 0x11 # Engine Manufacturer Diagnostics

M1P53_ERROR = 0xFF # Invalid value received for fuel type
M1P53_GASOLINE = 0x01 # Gasoline
M1P53_METHANOL = 0x02 # Methanol
M1P53_ETHANOL = 0x03 # Ethanol
M1P53_DIESEL = 0x04 # Diesel
M1P53_LPG = 0x05 # LPG
M1P53_CNG = 0x06 # CNG
M1P53_PROPANE = 0x07 # Propane
M1P53_ELECTRIC = 0x08 # Electric
M1P53_BI_GAS = 0x09 # Bifuel running Gasoline
M1P53_BI_METH = 0x0A # Bifuel running Methanol
M1P53_BI_ETH = 0x0B # Bifuel running Ethanol
M1P53_BI_LPG = 0x0C # Bifuel running LPG
M1P53_BI_CNG = 0x0D # Bifuel running CNG
M1P53_BI_PROP = 0x0E # Bifuel running Propane
M1P53_BI_ELEC = 0x0F # Bifuel running Electricity
M1P53_GAS_ELEC = 0x10 # Bifuel running Gasoline / Electric
M1P53_HY_GAS = 0x11 # Hybrid Gasoline
M1P53_HY_ETH = 0x12 # Hybrid Ethanol
M1P53_HY_DIESEL = 0x13 # Hybrid Diesel
M1P53_HY_ELEC = 0x14 # Hybrid Electric
M1P53_HY_MIXED = 0x15 # Hybrid mixed fuel
M1P53_HY_REGEN = 0x16 # Hybrid Regenerative

M1_P65_UNSUPP = 0 # Auxilliary I/O is unsupported
M1_P65_SUPP_INACTIVE = 1 # Auxilliary I/O is supported, but inactive
M1_P65_SUPP_ACTIVE = 2 # Auxilliary I/O is supported and active


def PIDs_Supported(interface):
    """
    Requests the PIDs supported via the given interface face
    
    Params
    interface -- Interface to use when communicating with the car
    
    Returns
    Array of booleans where each index match a PID. The item is True if
    the PID is supported otherwise it will be false
    """
    
    retVal = [True]
    noMorePIDs = False
    
    for i in range(4):
        #If no more are supported just set pidList to 0 and don't waste
        #time doing anymore polling
        if noMorePIDs:
            pidList = 0x00000000
        else:
            pidList = PID_Supported(interface.request_data(i * 0x20));
            
        for i in range(0x20):
            if (pidList & (1 << i)) != 0:
                retVal.append(True)
            else:
                retVal.append(False)
                if i == 0x1F:
                    #The last bit in each block tells if the next block
                    #is supported.
                    noMorePIDs = True
    

def OCS_PID_Translate(pid, data):
    """
    Translates a the data received from a PID request into useful
    information
    
    Params
    pid -- PID to translate
    data -- Data received from PID request to translate
    
    Returns
    Translated data. Type of data varies upon PID
    """
    
    if pid < 0x20:
        return OCS_PID_Translate_00(pid, data)
    elif pid < 0x40:
        return OCS_PID_Translate_20(pid, data)
    elif pid < 0x60:
        return OCS_PID_Translate_40(pid, data)
    elif pid < 0x80:
        return OCS_PID_Translate_60(pid, data)
    else:
        return 0

def OCS_PID_Translate_00(pid, data):
    """
    Translates a the data received from a PID request into useful
    information. Translates PIDs 0x00 - 0x1F.
    
    Params
    pid -- PID to translate (0x00 - 0x1F are supported)
    data -- Data received from PID request to translate
    
    Returns
    Translated data. Type of data varies upon PID
    """
    
    if pid == 0x00: #PIDs supported [0x01 - 0x20]
        return PID_Supported(data)
    elif pid == 0x01: #Monitor status since DTCs cleared
        return PID_M1P01(data, 0x01)
    elif pid == 0x02: #Freeze DTC, unknown
        return data
    elif pid == 0x03: #Fuel System Status
        return PID_M1P03(data)
    elif pid == 0x04: #Calculated Engine Load, %
        return float(data) * 100 / 255
    elif pid == 0x05: #Engine Coolant Temperature, C
        return data - 40
    elif (pid == 0x06) or (pid == 0x07) or (pid == 0x08) or (pid == 0x09): 
        #Bank 1 Short Term Fuel % Trim, %
        #Bank 1 Long Term Fuel % Trim, %
        #Bank 2 Short Term Fuel % Trim, %
        #Bank 2 Long Term Fuel % Trim, %
        return float(data - 128) * 100 / 128
    elif pid == 0x0A: #Fuel Pressure, kPa (gauge)
        return data * 3
    elif pid == 0x0B: #Intake Manifold Absolute Pressure, kPa (Absolute)
        return data
    elif pid == 0x0C: #Engine RPM
        return float(data) / 4
    elif pid == 0x0D: #Vehicle Speed, km/h
        return data
    elif pid == 0x0E: #Timing Advance, w.r.t. Cylinder 1, Degree
        return float(data)/2 - 64
    elif pid == 0x0F: #Intake Air Temperature, C
        return data - 40
    elif pid == 0x10: #Mass Air Flow Rate, g/s
        return float(data) / 100
    elif pid == 0x11: #Throttle Position
        return float(data) * 100 / 255
    elif pid == 0x12: #Commanded Secondary Air Status
        return PID_M1P12(data)
    elif pid == 0x13: #Oxygen Sensors Present, Bank 1 Sensor 1 - B2S4
        return PID_M1P13(data)
    elif (pid == 0x14) or (pid == 0x15) or (pid == 0x16) or (pid == 0x17) or \
        (pid == 0x18) or (pid == 0x19) or (pid == 0x1A) or (pid == 0x1B): 
        #Oxygen sensor data, V, %
        return PID_Oxygen_Sensor(data)
    elif pid == 0x1C: # Vehicle ODB Standard 
        return PID_M1P1C(data)
    elif pid == 0x1D: # Oxygen Sensors Present, Bank 1 Sensor 1 - B4S2
        return PID_M1P1D(data)
    elif pid == 0x1E: # Auxiliary Input Status, PTO active
        return {'PTO':(data & 1) == 1}
    elif pid == 0x1F: # Run Time Since Engine Start, s
        return data
    else:
        return False
    
def OCS_PID_Translate_20(pid, data):
    """
    Translates a the data received from a PID request into useful
    information. Translates PIDs 0x20 - 0x3F.
    
    Params
    pid -- PID to translate (0x20 - 0x3F are supported)
    data -- Data received from PID request to translate
    
    Returns
    Translated data. Type of data varies upon PID
    """
    
    if pid == 0x20: # PIDs supported [0x21 - 0x40]
        return PID_Supported(data)
    elif pid == 0x21: #Distance Traveled with the MIL On, km
        return data
    elif pid == 0x22: #Fuel Rail Pressure Relative to Manifold Vacuum, kPa
        return float(data) * 0.079
    elif pid == 0x23: #Fuel Rail Pressure (Diesel)
        return data * 10
    elif (pid == 0x24) or (pid == 0x25) or (pid == 0x26) or (pid == 0x27) or \
        (pid == 0x28) or (pid == 0x29) or (pid == 0x2A) or (pid == 0x2B): 
        #Oxygen Sensor WR Lambda, Equivalence Ration and Voltage
        return PID_Oxygen_Lambda(data, voltage = True)
    elif pid == 0x2C: #Commanded EGR, %
        return float(data) * 100 / 255
    elif pid == 0x2D: #EGR Error, %
        return float(data - 128) * 100 / 128
    elif pid == 0x2E: #Commanded Evaporative Purge, %
        return float(data) * 100 / 255
    elif pid == 0x2F: #Fuel Level Input, %
        return float(data) * 100 / 255
    elif pid == 0x30: #Number of warm-ups since codes cleared 
        return data
    elif pid == 0x31: #Distance traveled since codes cleared, km
        return data
    elif pid == 0x32: #Evaporative System Pressure, Pa
        #0x8000 / 4 = -8192 Pa
        #0x7FFF / 4 = +8191.75 Pa
        if data >= 0x8000: #Is it signed?
            data = data & 0x7FFF #Remove negative sign
            data = data - 32768 #Make 0x0000 the lowest value
         
        return float(data) / 4 
    elif pid == 0x33: #Barometric Pressure, kPa (Absolute)
        return data
    elif (pid == 0x34) or (pid == 0x35) or (pid == 0x36) or (pid == 0x37) or \
        (pid == 0x38) or (pid == 0x39) or (pid == 0x3A) or (pid == 0x3B): 
        #Oxygen Sensor WR Lambda, Equivalence Ration and mA
        return PID_Oxygen_Lambda(data, current = True)
    elif (pid == 0x3C) or (pid == 0x3D) or (pid == 0x3E) or (pid == 0x3F):
        #Catalyst Temperature Bank 1 Sensor 1, B2S1, B1S2, B2S2   
        return float(data) / 10 - 40
    else:
        return False
    
def OCS_PID_Translate_40(pid, data):
    """
    Translates a the data received from a PID request into useful
    information. Translates PIDs 0x40 - 0x5F.
    
    Params
    pid -- PID to translate (0x40 - 0x5F are supported)
    data -- Data received from PID request to translate
    
    Returns
    Translated data. Type of data varies upon PID
    """
    
    if pid == 0x40: # PIDs Supported [0x41 - 0x60]
        return PID_Supported(data)
    elif pid == 0x41: #Monitor status this drive cycle
        return PID_M1P01(data, 0x41)
    elif pid == 0x42: #Control module voltage, V
        return float(data) / 1000
    elif pid == 0x43: #Absolute load value, %
        return float(data) * 100 / 255
    elif pid == 0x44: #Common Equivalence Ratio, %
        return float(data) / 32768
    elif pid == 0x45: #Relative Throttle Position, %
        return float(data) * 100 / 255
    elif pid == 0x46: #Ambient Air Temperature, C
        return data - 40
    elif (pid == 0x47) | (pid == 0x48) | (pid == 0x49) | (pid == 0x4A) | \
        (pid == 0x4B): #Absolute Throttle Position B - F, %
        return float(data) * 100 / 255
    elif pid == 0x4C: #Commanded Throttle Actuator, %
        return float(data) * 100 / 255
    elif pid == 0x4D: #Time Run with MIL on, minutes
        return data
    elif pid == 0x4E: #Time Since Trouble Codes Cleared, minutes
        return data
    elif pid == 0x4F:   #Maximimum Values for Equivalence Ratio (%), 
                        #Oxygen Sensor Voltage (V),
                        #Oxygen Sensor Current (mA), 
                        #Intake Manifold Absolute Pressure (kPA)  
        return {'ER': int(data & 0xFF),\
                'O2V': int((data >> 8) & 0xFF),\
                'O2A': int((data >> 16) & 0xFF),\
                'MAP': int((data >> 24) & 0xFF) * 10}
    elif pid == 0x50: #Maximum Value for Air Flow Rate from MAF (g/s)
        return {'MAF': int(data & 0xFF) * 10}
    elif pid == 0x51: #Fuel type
        return PID_M1P51(data)
    elif pid == 0x52: #Ethanol Fuel %
        return float(data) * 100 / 255
    elif pid == 0x53: #Absolute Evap System Vapor Pressure, kPa 
        return float(data) / 200
    elif pid == 0x54: #Evap System Vapor Pressure, Pa
        return data - 32767
    elif (pid == 0x55) or (pid == 0x56) or (pid == 0x57) or (pid == 0x58): 
                        #Secondary Oxygen Sensor Trim, %
                        # Short Term Bank 1 and 3
                        # Long Term Bank 1 and 3
                        # Short Term Bank 2 and 4
                        # Long Term Bank 2 and 4
                        
        return [(float(data & 0xFF) - 128) * 100 / 128, 
                (float(data >> 8) - 128) * 100 / 128]
    elif pid == 0x59: #Fuel Rail Pressure, kPa
        return data * 10
    elif pid == 0x5A: #Relative Accelerator Pedal Position, %
        return float(data) * 100 / 255
    elif pid == 0x5B: #Hybrid Battery Pack Remaining Life, %
        return float(data) * 100 / 255
    elif pid == 0x5C: #Engine Oil Temperature, C
        return data - 40
    elif pid == 0x5D: #Fuel Injection Timing, Degrees
        return float(data - 26880) / 128
    elif pid == 0x5E: #Engine Fuel Rate
        return float(data) * 0.05
    elif pid == 0x5F: #Emission Requirements to Which the Vehicle is Designed
        return PID_M1P5F(data)
    else:
        return False
    
def OCS_PID_Translate_60(pid, data):
    """
    Translates a the data received from a PID request into useful
    information. Translates PIDs 0x60 - 0x7F.
    
    Params
    pid -- PID to translate (0x60 - 0x7F are supported)
    data -- Data received from PID request to translate
    
    Returns
    Translated data. Type of data varies upon PID
    """
    
    if pid == 0x60: # PIDs supported [0x61 - 0x80]
        return PID_Supported(data)
    elif pid == 0x61: # Driver's Demand Engine Percent Torque, %
        return data - 125
    elif pid == 0x62: # Actual Engine Percent Torque, %
        return data - 125
    elif pid == 0x63: # Engine Reference Torque, %
        return data
    elif pid == 0x64: # Engine Percent Torque Data, %
        #Returns [idle, Engine Point 1, EP2, EP3, EP4]
        #retVal = []
        #for i in range(5):
        #    retVal.insert(0, int(data & 0xFF))
        #    data = data >> 8
        #return retVal
        return {'Idle': int((data >> 32  & 0xFF) - 125),
                'EP1': int(((data >> 24) & 0xFF) - 125),
                'EP2': int(((data >> 16) & 0xFF) - 125),
                'EP3': int(((data >>  8) & 0xFF) - 125),
                'EP4': int(((data      ) & 0xFF) - 125)}
    elif pid == 0x65: # Auxilliary Input / Output Supported
        return PID_M1_P65(data)
    elif pid == 0x66: # Mass Air Flow Sensor
        retVal = {'MAFA': False, 'MAFB': False}
        if data & 0x0100000000 > 0:
            retVal['MAFA'] = float((data >> 16) & 0xFFFF) * 0.03125
        if data & 0x0200000000 > 0:
            retVal['MAFB'] = float((data      ) & 0xFFFF) * 0.03125
        return retVal
    elif pid == 0x67: # Engine Coolant Temperature
        retVal = {'ECT1': False, 'ECT2': False}
        if data & 0x010000 > 0:
            retVal['ECT1'] = int((data >> 8) & 0xFF) - 40
        if data & 0x020000 > 0:
            retVal['ECT2'] = int((data      ) & 0xFF) - 40
        return retVal
    #elif pid == 0x68: # Intake Air Temperature Sensor
    #    return
    #elif pid == 0x69: # Commanded EGR and EGR Error
    #    return
    #elif pid == 0x6A: # Commanded Diesel Intake Air Flow Control and
    #                  # Relative Intake Air Flow Position
    #    return
    #elif pid == 0x6B: # Exhaust Gas Recirculation Temperature
    #    return
    #elif pid == 0x6C: # Commanded Air Actuator Control and
    #                  # Relative Throttle Position
    #    return
    #elif pid == 0x6D: # Fuel Pressure Control System
    #    return
    #elif pid == 0x6E: # Injection Pressure Control System
    #    return
    #elif pid == 0x6F: # Tubocharger Compressor Inlet Pressure
    #    return
    #elif pid == 0x70: # Boost Pressure Control
    #    return
    #elif pid == 0x71: # Variable Geometry Turbo (VGT) Control
    #    return
    #elif pid == 0x72: # Waste Gate Control
    #    return
    #elif pid == 0x73: # Exhaust Pressure
    #    return
    #elif pid == 0x74: # Turbocharger RPM
    #    return
    #elif pid == 0x75: # Turbocharger Temperature
    #    return
    #elif pid == 0x76: # Turbocharger Temperature
    #    return
    #elif pid == 0x77: # Charge Air Cooler Temperature (CACT)
    #    return
    #elif pid == 0x78: # Exhaust Gas Temeprature (EGT) Bank 1
    #    return
    #elif pid == 0x79: # Exhaust Gas Temperature (EGT) Bank 2
    #    return
    #elif pid == 0x7A: # Diesel Particulate Filter (DPF)
    #    return
    #elif pid == 0x7B: # Diesel Particulate Filter (DPF)
    #    return
    #elif pid == 0x7C: # Diesel Particulate Filter (DPF)
    #    return
    #elif pid == 0x7D: # NOx NTE Control Area Status
    #    return
    #elif pid == 0x7E: # PM NTE Control Area Status
    #    return
    #elif pid == 0x7F: # Engine Run Time
    #    return
    else:
        return False
    
            
def PID_Supported(data):
    """
    Returns a 32-bit value representing the supported bits in a range.
    
    Bits are rearranged from original order of bytes ABCD representing 
    PIDs [09-01][12-0A][1B-13][20-1C] to [20 - 01]
    
    Params
    data -- Value returned from a Mode 1 OBD-II request for PIDs 0x00, 
        0x20, 0x40, 0x60, and 0x80
        
    Return
    Rearranged supported PID list
    """
    
    #[20 - 1C] 00 00 00
    tempData = (data & 0xFF) << 24
    #[20 - 1C] [1B - 13] 00 00
    tempData = tempData | ((data & 0xFF00) << 8)
    #[20 - 1C] [1B - 13] [12 - 0A] 00
    tempData = tempData | ((data & 0xFF0000) >> 8)
    #[20 - 1C] [1B - 13] [12 - 0A] [40 - 21]
    tempData = tempData | ((data & 0xFF0000) >> 24)
    
    return tempData
        
def PID_M1P01(data, pid):
    """
    Parses a Mode 1, PID 0x01, OBDII response with the following format.
    Since Mode 1, PID 0x41 uses the same byte pattern as PID 0x01, this
    function parses that PID as well.
    
    Byte A is Excluded in PID 0x41
    A0-A6 DTC_CNT   Number of confirmed emissions-related DTCs available
    A7    MIL       Is the check engine light on?
    
    B3    No Name   0 - Spark ignition monitors supported
                    1 - Compression ignition monitors supports
                    Excluded in PID 0x41
    
    B6              Component test incomplete
    B5              Fuel system test incomplete
    B4              Misfire test incomplete
    B2              Component Test Supported
    B1              Fuel system Test Supported
    B0              Misfire Test Supported
    
    When B3 = 0; Spark ignition monitors
    D7              EGR System Test Incomplete
    D6              Oxygen Sensor Heater Test Incomplete
    D5              Oxygen Sensor Test Incomplete
    D4              A/C Refrigerant Test Incomplete
    D3              Secondary Air System Test Incomplete
    D2              Evaporative Test Incomplete
    D1              Heated Catalyst Test Incomplete
    D0              Catalyst Test Incomplete
    
    C7              EGR System Test Supported
    C6              Oxygen Sensor Heater Test Supported
    C5              Oxygen Sensor Test Supported
    C4              A/C Refrigerant Test Supported
    C3              Secondary Air System Test Supported
    C2              Evaporative Test Supported
    C1              Heated Catalyst Test Supported
    C0              Catalyst Test Supported
    
    When B3 = 1; Compression ignition monitors
    D7              EGR and/or VVT Test Incomplete
    D6              PM Filter Monitoring Test Incomplete        
    D5              Exhaust Gas Sensor Test Incomplete
    D3              Boot Pressure Test Incomplete
    D1              NOx / SCR Monitor Test Incomplete
    D0              NMHC Test Incomplete
    
    C7              EGR and/or VVT Test Supported
    C6              PM Filter Monitoring Test Supported        
    C5              Exhaust Gas Sensor Test Supported
    C3              Boot Pressure Test Supported
    C1              NOx / SCR Monitor Test Supported
    C0              NMHC Test Supported
    
    Params
        -- data 4-byte value receive from OBD-II bus for a Mode 1, 
            PID 0x01 or 0x41 request
        -- PID being checked 0x01 or 0x41 are valid. Other values will
            return False
    
    Returns
        -- False if the PID supplied is not 0x01 or 0x41
        -- ['DTC_CNT'] Number of confirmed emissions-related DTCs available
        -- ['MIL'] Should the check engine light be on?
        -- ['MONITOR'] Spark ignition (0) or compression ignition (1)
            -- TEST_NOT_SUPPORTED - Test is not supported
            -- TEST_SUPPORTED_COMP - Test was completed
            -- TEST_SUPPORTED_INCOMP - Test was incomplete
            
            --Additional fields will only be present when their 
            respective ignition system is present
            
            -- Spark ignition additional test fields
            -- ['EGR'] EGR System
            -- ['O2_HEAT'] Oxygen Sensor Heater
            -- ['O2_SENSOR'] Oxygen Sensor
            -- ['AC'] A/C Refrigerant
            -- ['SEC_AIR'] Secondary Air System
            -- ['EVAP'] Evaporative System
            -- ['HEATED_CAT'] Heated Catalyst
            -- ['CAT'] Catalyst
            
            -- Compression ignition additional test fields
            -- ['EGR'] - EGR and / or VVT System
            -- ['PM'] - PM Filter Monitoring
            -- ['EX_GAS'] - Exhaust Gas Sensor
            -- ['NOX'] - NOx / SCR Monitor
            -- ['NMHC'] - NMHC Cat
    """
    if pid != 0x01 and pid != 0x41:
        return False
    
    retVal = {}
    if pid == 0x01:
        retVal['DTC_CNT'] = int((data >> 24) & 0b1111111)
        retVal['MIL'] = int((data >> 31) & 1)
        retVal['MONITOR'] = int((data >> 19) & 1)
    
    commonTestByteB = (data >> 16) & 0xFF
    supportedByteC = (data >> 8) & 0xFF
    incompleteByteD = (data & 0xFF)
    
    #Common Tests
    if commonTestByteB & 0x01 == 0:
        retVal['MISFIRE'] = TEST_NOT_SUPPORTED
    elif commonTestByteB & 0x10 == 0:
        retVal['MISFIRE'] = TEST_SUPPORTED_COMP
    else:
        retVal['MISFIRE'] = TEST_SUPPORTED_INCOMP
        
    if commonTestByteB & 0x02 == 0:
        retVal['FUEL_SYS'] = TEST_NOT_SUPPORTED
    elif commonTestByteB & 0x20 == 0:
        retVal['FUEL_SYS'] = TEST_SUPPORTED_COMP
    else:
        retVal['FUEL_SYS'] = TEST_SUPPORTED_INCOMP
        
    if commonTestByteB & 0x04 == 0:
        retVal['COMPS'] = TEST_NOT_SUPPORTED
    elif commonTestByteB & 0x40 == 0:
        retVal['COMPS'] = TEST_SUPPORTED_COMP
    else:
        retVal['COMPS'] = TEST_SUPPORTED_INCOMP
        
    
    if pid == 0x41 or retVal['MONITOR'] == 0:
        #Spark Ignition Tests
        if supportedByteC & 0x80 == 0:
            retVal['EGR'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x80 == 0:
            retVal['EGR'] = TEST_SUPPORTED_COMP
        else:
            retVal['EGR'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x40 == 0:
            retVal['O2_HEAT'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x40 == 0:
            retVal['O2_HEAT'] = TEST_SUPPORTED_COMP
        else:
            retVal['O2_HEAT'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x20 == 0:
            retVal['O2_SENSOR'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x20 == 0:
            retVal['O2_SENSOR'] = TEST_SUPPORTED_COMP
        else:
            retVal['O2_SENSOR'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x10 == 0:
            retVal['AC'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x10 == 0:
            retVal['AC'] = TEST_SUPPORTED_COMP
        else:
            retVal['AC'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x08 == 0:
            retVal['SEC_AIR'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x08 == 0:
            retVal['SEC_AIR'] = TEST_SUPPORTED_COMP
        else:
            retVal['SEC_AIR'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x04 == 0:
            retVal['EVAP'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x04 == 0:
            retVal['EVAP'] = TEST_SUPPORTED_COMP
        else:
            retVal['EVAP'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x02 == 0:
            retVal['HEATED_CAT'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x02 == 0:
            retVal['HEATED_CAT'] = TEST_SUPPORTED_COMP
        else:
            retVal['HEATED_CAT'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x01 == 0:
            retVal['CAT'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x01 == 0:
            retVal['CAT'] = TEST_SUPPORTED_COMP
        else:
            retVal['CAT'] = TEST_SUPPORTED_INCOMP
            
    else:
        if supportedByteC & 0x80 == 0:
            retVal['EGR'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x80 == 0:
            retVal['EGR'] = TEST_SUPPORTED_COMP
        else:
            retVal['EGR'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x40 == 0:
            retVal['PM'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x40 == 0:
            retVal['PM'] = TEST_SUPPORTED_COMP
        else:
            retVal['PM'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x20 == 0:
            retVal['EXH_GAS'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x20 == 0:
            retVal['EXH_GAS'] = TEST_SUPPORTED_COMP
        else:
            retVal['EXH_GAS'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x08 == 0:
            retVal['BOOST'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x08 == 0:
            retVal['BOOST'] = TEST_SUPPORTED_COMP
        else:
            retVal['BOOST'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x02 == 0:
            retVal['NOX'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x02 == 0:
            retVal['NOX'] = TEST_SUPPORTED_COMP
        else:
            retVal['NOX'] = TEST_SUPPORTED_INCOMP
            
        if supportedByteC & 0x01 == 0:
            retVal['NMHC'] = TEST_NOT_SUPPORTED
        elif incompleteByteD & 0x01 == 0:
            retVal['NMHC'] = TEST_SUPPORTED_COMP
        else:
            retVal['NMHC'] = TEST_SUPPORTED_INCOMP
            
    return retVal

def PID_M1P03(data):
    """
    Takes an OBD-II Mode 1, PID 0x03 value and returns status of both 
    banks of the fuel system in a two field array.
    
    Returns:
    Two item array with the following values, one for each bank
        M1P03_NONE - No valid data received for given bank
        M1P03_OPEN_TEMP - Open loop due to insufficient engine 
                            temperature
        M1P03_CLOSED_O2 - Closed loop, using oxygen sensor feedback to 
                            determine fuel mix
        M1P03_OPEN_LOAD - Open loop due to engine load OR fuel cut due 
                            to decceleration
        M1P03_OPEN_FAULT - Open loop due to system failure
        M1P03_CLOSED_FAULT - Closed loop, using at least one oxygen 
                             sensor but there is a fault in the 
                             feedback system
    
    """
    retVal = [M1P03_NONE, M1P03_NONE]
    for bit in range(16):
        if data & (1 << bit) != 0:
            if bit == 0:
                retVal[0] = M1P03_OPEN_TEMP
            elif bit == 1:
                retVal[0] = M1P03_CLOSED_O2
            elif bit == 2:
                retVal[0] = M1P03_OPEN_LOAD
            elif bit == 3:
                retVal[0] = M1P03_OPEN_FAULT
            elif bit == 4:
                retVal[0] = M1P03_CLOSED_FAULT
            elif bit == 8:
                retVal[1] = M1P03_OPEN_TEMP
            elif bit == 9:
                retVal[1] = M1P03_CLOSED_O2
            elif bit == 10:
                retVal[1] = M1P03_OPEN_LOAD
            elif bit == 11:
                retVal[1] = M1P03_OPEN_FAULT
            elif bit == 12:
                retVal[1] = M1P03_CLOSED_FAULT
    return retVal

def PID_M1P12(data):
    """
    Takes an OBD-II Mode 1 PID 12 value and returns the secondary air 
    status
    
    Return Values
    M1P12_NONE - No valid data received
    M1P12_UPSTREAM - Upstream of catalytic converter
    M1P12_DOWNSTREAM - Downstream of catalytic converter  
    M1P12_OUTSIDE_OFF - From the outside or off
    """
    retVal = M1P12_NONE
    
    if data == 0x01:
        retVal = M1P12_UPSTREAM
    elif data == 0x02:
        retVal = M1P12_DOWNSTREAM
    elif data == 0x04:
        retVal = M1P12_OUTSIDE_OFF
    return retVal
    
    return retVal

def PID_M1P13(data):
    """
    From the OBD-II Mode 1, PID 0x13 provided value, determines what 
    Oxygen sensors are present
    
    Returns
    Array of statuses for each Oxygen sensor. A sensor is present if the
    value is True. Index 0 is Bank 1 Sensor 1, Index 7 is Bank 2 Sensor 4
    """

    retVal = []
    for bit in range(8):
        if data & (1 << bit) > 0:
            retVal.append(True)
        else:
            retVal.append(False)

    return retVal

def PID_Oxygen_Sensor(data):
    """
    From the data provided by an Oxygen sensor reading, the sensor voltage and 
    short term fuel trim will be returned
    
    Returns
    ['Voltage'] -- Oxygen sensor voltage, V
    ['Trim'] -- Short Term Fuel Trim, %
    """
       
    return {'Voltage': float(data >> 8) / 200, \
              'Trim': (float(data & 0xFF) - 128) * 100 / 128}

def PID_M1P1C(data):
    """
    Takes the data from an ODB-II Mode 1 PID 1C request and determines 
    the OBD-II compliance level
    
    Returns
    OBD_TYPE_ERROR - Invalid value received
    OBD_TYPE_CARB - OBD-II as defined by the CARB 
    OBD_TYPE_EPA - OBD as defined by the EPA
    OBD_TYPE_12 - OBD and OBD-II
    OBD_TYPE_1 - OBD-I
    OBD_TYPE_NON - Not meant to comply with any OBD Standard 
    OBD_TYPE_E - EOBD (Europe)
    OBD_TYPE_E2 - EOBD (Europe) and OBD 
    OBD_TYPE_E1 - EOBD (Europe) and OBD-II
    OBD_TYPE_E12 - EOBD (Europe), OBD, and OBD-II
    OBD_TYPE_J - JOBD (Japan)
    OBD_TYPE_J2 - JOBD (Japan) and OBD-II
    OBD_TYPE_JE - JOBD (Japan) and EOBD (Europe)
    OBD_TYPE_JE2 - JOBD (Japan), EOBD (Europe), and OBD-II
    OBD_TYPE_EURO_IV_B1 -- Heavy Duty Vehicles (Euro IV) B1
    OBD_TYPE_EURO_V_B2 -- Heavy Duty Vehicles (Euro V) B2
    OBD_TYPE_EURO_EEV_C -- Heavy Du1ty Vehicles (Euro EEV) C
    """
    
    #Returned values are  the same as those received from the OBD-II bus
    if data > 0 and data <= OBD_TYPE_EMD:
        return data
    else:
        return OBD_TYPE_ERROR
    
def PID_M1P1D(data):
    """
    From the OBD-II Mode 1, PID 0x1D provided value, determines what 
    Oxygen sensors are present
    
    Returns
    Array of statuses for each Oxygen sensor. A sensor is present if the
    value is True. Index 0 is Bank 1 Sensor 1, Index 7 is Bank 4 Sensor 2
    """

    retVal = []
    for bit in range(8):
        if data & (1 << bit) > 0:
            retVal.append(True)
        else:
            retVal.append(False)

    return retVal

def PID_Oxygen_Lambda(data, voltage=False, current=False):
    """
    From the OBD-II Mode 1, PID 0x24 - 0x2B or PID 0x34 - 0x3B for 
    Oxygen Sensors 1 - 8 WR lambda returns the Equivalence Ratio and 
    Voltage or Current depending on what was requested. 
    
    The parameters voltage and current must match their PID otherwise 
    the returned value will be garbage.
    
    Params
    data -- Data received from the OBD-II bus
    voltage -- Data contains voltage information (Default: False)
    current -- Data contains current information (Default: False)
    
    Returns
    Array of the Oxygen Sensor WR Lambda values
    ['ratio'] -- Equivalence Ratio
    ['voltage'] -- Voltage, V
    ['current'] -- Current, mA
    """
    
    retVal = {'Ratio': float(data & 0xFFFF) * 2 / 65535}
    
    if voltage:
        retVal['Voltage'] = float((data >> 16) & 0xFFFF) * 8 / 65535
    elif current:
        retVal['Current'] = float((data >> 16) & 0xFFFF) * 128 / 32768 - 128

    return retVal

def PID_M1P51(data):
    """
    Returns the currently used fuel type
    
    Params
    data -- Data  received from the OBD-II bus
    
    Returns
    M1P53_ERROR -- Invalid value received for fuel type
    M1P53_GASOLINE -- Gasoline
    M1P53_METHANOL -- Methanol
    M1P53_ETHANOL -- Ethanol
    M1P53_DIESEL -- Diesel
    M1P53_LPG -- LPG
    M1P53_CNG -- CNG
    M1P53_PROPANE -- Propane
    M1P53_ELECTRIC -- Electric
    M1P53_BI_GAS -- Bifuel running Gasoline
    M1P53_BI_METH -- Bifuel running Methanol
    M1P53_BI_ETH -- Bifuel running Ethanol
    M1P53_BI_LPG -- Bifuel running LPG
    M1P53_BI_CNG -- Bifuel running CNG
    M1P53_BI_PROP -- Bifuel running Propane
    M1P53_BI_ELEC -- Bifuel running Electricity
    M1P53_GAS_ELEC -- Bifuel running Gasoline / Electric
    M1P53_HY_GAS -- Hybrid Gasoline
    M1P53_HY_ETH -- Hybrid Ethanol
    M1P53_HY_DIESEL -- Hybrid Diesel
    M1P53_HY_ELEC -- Hybrid Electric
    M1P53_HY_MIXED -- Hybrid mixed fuel
    M1P53_HY_REGEN -- Hybrid Regenerative
    """
    if data > 0x00 and data <= 0x0F:
        return data
    else:
        return M1P53_ERROR

def PID_M1P5F(data):
    """
    Emission Requirements to Which Vehicle is Designed. Only handles
    values of 0x0E - 0x10. Might need to handle those less than 0x0E
    since SAE1979 2006 states that the data was previously contained in
    PID 0x1C
    
    Params
    data -- Data  received from the OBD-II bus
    
    Returns
    OBD_TYPE_EURO_IV_B1 -- Heavy Duty Vehicles (Euro IV) B1
    OBD_TYPE_EURO_V_B2 -- Heavy Duty Vehicles (Euro V) B2
    OBD_TYPE_EURO_EEV_C -- Heavy Du1ty Vehicles (Euro EEV) C
    OBD_TYPE_ERROR -- Value was out of range
    
    """
    if data >= 0x0E and data <= 0x10:
        return data
    else:
        return OBD_TYPE_ERROR
    
def PID_M1_P65(data):
    """
    Auxilliary Input / Output Decoder. Outputs the status of the
    Power Take Off, Automatic Transmission Neutral State (vs. In Drive),
    Manual Transmission Neutral State (vs. In Gear), and
    Glow Plug Lamps Status
    
    Params
    data -- Data  received from the OBD-II bus
    
    Returns
    Array with the following fields,
    {'PTO', 'Auto_Neutral', 'Manual_Neutral', 'Glow'},
    and their possible values,
    M1_P65_UNSUPP -- Auxilliary I/O is unsupported
    M1_P65_SUPP_INACTIVE -- Auxilliary I/O is supported, but inactive
    M1_P65_SUPP_ACTIVE -- Auxilliary I/O is supported and active
    """
    
    retVal = {}
    
    if (data & 0x0100) == 0:
        retVal['PTO'] = M1_P65_UNSUPP
    elif (data & 0x0001) == 0:
        retVal['PTO'] = M1_P65_SUPP_INACTIVE
    else:
        retVal['PTO'] = M1_P65_SUPP_ACTIVE
        
    if (data & 0x0200) == 0:
        retVal['Auto_Neutral'] = M1_P65_UNSUPP
    elif (data & 0x0002) == 0:
        retVal['Auto_Neutral'] = M1_P65_SUPP_INACTIVE
    else:
        retVal['Auto_Neutral'] = M1_P65_SUPP_ACTIVE
        
    if (data & 0x0200) == 0:
        retVal['Manual_Neutral'] = M1_P65_UNSUPP
    elif (data & 0x0002) == 0:
        retVal['Manual_Neutral'] = M1_P65_SUPP_INACTIVE
    else:
        retVal['Manual_Neutral'] = M1_P65_SUPP_ACTIVE
        
    if (data & 0x0400) == 0:
        retVal['Glow'] = M1_P65_UNSUPP
    elif (data & 0x0004) == 0:
        retVal['Glow'] = M1_P65_SUPP_INACTIVE
    else:
        retVal['Glow'] = M1_P65_SUPP_ACTIVE
        
    return retVal

if __name__ == '__main__':
    for i in range(0, 0x5F):
        pid = raw_input("Enter PID: ")
        pid = int(pid, 16)
        print PID_TABLE[pid][0]
        data = raw_input("Enter value for PID: ")
        data = int(data, 16)
        print OCS_PID_Translate(pid, data)