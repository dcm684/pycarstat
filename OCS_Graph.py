"""
Based on code by Edward Abraham & John Bender 
found at http://www.scipy.org/Matplotlib_figure_in_a_wx_panel
"""

import matplotlib
matplotlib.interactive(True)
matplotlib.use('WXAgg')

import numpy as num
import wx

class PlotPanel (wx.Panel):
    """The PlotPanel has a Figure and a Canvas. OnSize events simply 
    set a flag, and the actual resizing of the figure is triggered by 
    an Idle event."""
    def __init__(self, parent, color=None, dpi=None, **kwargs):
        from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
        from matplotlib.figure import Figure

        # initialize Panel
        if 'id' not in kwargs.keys():
            kwargs['id'] = wx.ID_ANY
        if 'style' not in kwargs.keys():
            kwargs['style'] = wx.NO_FULL_REPAINT_ON_RESIZE
        wx.Panel.__init__(self, parent, **kwargs)

        # initialize matplotlib stuff
        self.figure = Figure(None, dpi)
        self.canvas = FigureCanvasWxAgg(self, -1, self.figure)
        self.SetColor(color)

        self._SetSize()
        self.draw()

        self._resizeflag = False

        self.Bind(wx.EVT_IDLE, self._onIdle)
        self.Bind(wx.EVT_SIZE, self._onSize)

    def SetColor(self, rgbtuple=None):
        """Set figure and canvas colours to be the same."""
        if rgbtuple is None:
            rgbtuple = wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNFACE).Get()
        clr = [c/255. for c in rgbtuple]
        self.figure.set_facecolor(clr)
        self.figure.set_edgecolor(clr)
        self.canvas.SetBackgroundColour(wx.Colour(*rgbtuple))

    def _onSize(self, event):
        self._resizeflag = True

    def _onIdle(self, evt):
        if self._resizeflag:
            self._resizeflag = False
            self._SetSize()

    def _SetSize(self):
        """
        Sets the size of the plot panel
        
        If the size is smaller than the minimum or larger than the
        maximimum, that dimension is resized to the min or max that it
        violated
        """
        
        try:
            pixels = tuple(self.parent.mySizer.GetItem(0).GetSize())
        except:
            pixels = tuple(self.parent.GetClientSize())

        if pixels[0] < self.MinWidth:
            pixels = (self.MinWidth, pixels[1])
        
        if pixels[1] < self.MinHeight:
            pixels = (pixels[0], self.MinHeight)
            
        if self.MaxWidth > -1:
            if pixels[0] > self.MaxWidth:
                pixels = (self.MaxWidth, pixels[1])
        
        if self.MaxHeight > -1:
            if pixels[1] > self.MaxHeight:
                pixels = (pixels[0], self.MaxHeight)

        self.SetSize(pixels)
        self.canvas.SetSize(pixels)
        self.figure.set_size_inches(float(pixels[0])/self.figure.get_dpi(),
                                     float(pixels[1])/self.figure.get_dpi())

    def draw(self): pass # abstract, to be overridden by child classes
    
class LivePlotPanel (PlotPanel):
    def __init__(self, parent, point_list, clr_list, **kwargs):
        self.parent = parent
        
        if point_list == False:
            self.pointList = [[]]
        else:
            self.pointList = point_list
        self.colorList = clr_list

        self.title = ""
        self.xAxisTitle = ""
        self.yAxisTitle = ""
        
        self.lineList = [0]*len(self.pointList)
        
        PlotPanel.__init__(self, parent, **kwargs)
        self.SetColor((255,255,255))
        
    def draw(self):
        if not hasattr(self, 'subplot'):
            self.subplot = self.figure.add_subplot(1,1,1)
            
        self.subplot.clear()

        #Title and axis labels are deleted when subplot is cleared
        self.subplot.set_title(self.title)
        self.subplot.set_xlabel(self.xAxisTitle)
        self.subplot.set_ylabel(self.yAxisTitle)

        
        for i, aList in enumerate(self.pointList):
            if len(aList) > 0:
                plotPoints = num.array(aList)
                plotColor = [float(c)/255.0 for c in self.colorList[i]]
                self.lineList[i], = self.subplot.plot(plotPoints[:,0], 
                                                      plotPoints[:,1], 
                                                      color = plotColor)

    def setTitle(self,inTitle = "", inXAxis = "", inYAxis = ""):
        """Allow other functions to change the subplot and axis labels"""
        self.title = inTitle
        self.subplot.set_title(inTitle)

        if len(inXAxis) > 0:
            self.xAxisTitle = inXAxis
            self.subplot.set_xlabel(inXAxis)
            
        
        if len(inYAxis) > 0:
            self.yAxisTitle = inYAxis
            self.subplot.set_ylabel(inYAxis)
            
    def append(self, newPoints):
        """Adds newPoints to end of pointList and updates the plot
        
        New points can be an array or single point
        
        Points are in the for (x,y)
        """
        self.pointList[0].extend(newPoints)
        self.draw()
        self.canvas.draw()
        
    def clear(self):
        self.pointList = []
        
    def set_max_points(self, maxPoints):
        """
        Sets the maximum number of points that will be displayed and then
        redraws plot
        
        WARNING: This will delete those not within the most recent set
        """
        for i, aList in enumerate(self.pointList):
        
            aList = aList[len(aList) - maxPoints:]
            self.pointList[i] = aList
            
            #plotPoints = num.array(aList)
            #self.colorList[i] = [255,0,0]
            #self.lineList[i].set_xdata(plotPoints[:,0])
            #self.lineList[i].set_ydata(plotPoints[:,1])
            
        self.draw()
        self.canvas.draw()