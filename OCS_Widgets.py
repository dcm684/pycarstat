import OCS_Graph
import OCS_DataInterface as OCS_DInter
from OCS_DataInterface import UNIT_TEXT, PID_TABLE, PID_ACCEL_X, \
    PID_ACCEL_Y, PID_ACCEL_Z, PID_FUEL_ECON

import wx

PANEL_LINE_GRAPH = 1
PANEL_BAR_GRAPH = 2
PANEL_GAUGE = 11

class PIDgetSettings(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title)
        
        sizer = wx.GridBagSizer(5, 5)
        
        #Add PID selection
        sizer.Add(wx.StaticText(self, -1, 'Track:'), (0,0), \
                  flag = wx.ALIGN_LEFT)
        self.shownPIDs = [0x0D, 0x0C, PID_FUEL_ECON,
                          PID_ACCEL_X, PID_ACCEL_Y,
                          PID_ACCEL_Z]
        self.functionList = ['Vehicle Speed', 'Engine Speed', 'Fuel Economy',
                             'Horizontal Acceleration', 'Vertical Acceleration',
                             'Forward Acceleration']
        self.functionCombo = wx.ComboBox(self, -1, choices = self.functionList)
        
        sizer.Add(self.functionCombo, (0,1))

        #Add unit selection radio button
        sizer.Add(wx.StaticText(self, -1, 'Units:'), (1,0), \
                  flag = wx.ALIGN_LEFT)
        self.metRad = wx.RadioButton(self, -1,
                                     UNIT_TEXT[0][PID_TABLE[parent.pid][2]], \
                                     style = wx.RB_GROUP)
        sizer.Add(self.metRad, (1,1), \
                  flag = wx.ALIGN_LEFT)
        
        self.USRad = wx.RadioButton(self, -1,
                                    UNIT_TEXT[1][PID_TABLE[parent.pid][2]])
        sizer.Add(self.USRad, (1,2), \
                  flag = wx.ALIGN_LEFT)
        
        
        #Add graph line color change
        sizer.Add(wx.StaticText(self, -1, 'Line Color:'), (2,0), \
                  flag = wx.ALIGN_LEFT)
        
        #colorRect = 
        
        #Add pause checkbox getting value from context menu
        sizer.Add(wx.StaticText(self, -1, 'Pause:'), (3,0), \
                  flag = wx.ALIGN_LEFT)
        
        self.checkPause = wx.CheckBox(self, -1)
        #TODO: Re-reference pause check box in context menu
        self.checkPause.SetValue(parent.contextMenu.IsChecked(parent.menuItems[0][0]))
        sizer.Add(self.checkPause, (3,1), flag = wx.ALIGN_CENTER)
        
        #Add OK and Cancel buttons
        sizer.Add(wx.Button(self, wx.ID_OK, "OK"), (4,0), (1, 2), \
                  flag = wx.ALIGN_LEFT)
        self.Bind(wx.EVT_BUTTON, self._handle_ok, id = wx.ID_OK)
        
        sizer.Add(wx.Button(self, wx.ID_CANCEL, "Cancel"), (4,2), (1, 2), \
                  flag = wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_BUTTON, self._handle_cancel, id = wx.ID_CANCEL)
        
        self.SetSizerAndFit(sizer)
        self.Show()

    def _handle_ok(self, event):
        """Applies the users changes"""
        self.Close()
        
    def _handle_cancel(self, event):
        """Ignore users changes and close this window"""
        self.Close()
        

class PIDLivePlot(OCS_Graph.LivePlotPanel):
    def __init__(self, parent,
                 pid,
                 units = OCS_DInter.UNIT_METRIC,
                 point_list = False,
                 clr_list = [[0,0,0]], 
                 **kwargs):
        
        self.pid = pid
        self.pause = False
        self.colorList = clr_list
        
        if units == OCS_DInter.UNIT_US:
            self.units = units
        else:
            self.units = OCS_DInter.UNIT_METRIC
        
        OCS_Graph.LivePlotPanel.__init__(self, parent, point_list,  
                                         self.colorList, **kwargs)

        if PID_TABLE[pid][2] <= OCS_DInter.UNIT_KM:
            ylabel = OCS_DInter.UNIT_TEXT[self.units][PID_TABLE[pid][2]]
        else:
            ylabel = ""

        self.setTitle(PID_TABLE[pid][0], 
                      inXAxis = "seconds", inYAxis = ylabel)
        
        self.create_context_menu()
        #Set the click handler
        self.canvas.mpl_connect('button_press_event', self.mouse_button_press)
        
    def create_context_menu(self):
        self.menuItems = [
            [wx.NewId(), "Pause", None, wx.ITEM_CHECK],
            [wx.NewId(), "Remove", self.remove_handler, wx.ITEM_NORMAL],
            [wx.NewId(), "Settings", self.settings_handler, wx.ITEM_NORMAL]
        ]

        self.contextMenu = wx.Menu()
        for item in self.menuItems:
            self.contextMenu.Append (item[0], item[1], kind = item[3])
            wx.EVT_MENU(self.contextMenu, item[0], item[2])
        

    def mouse_button_press(self, event):
        """
        Handles mouse button presses on the canvas
        """
        
        if event.button == 3: #Right mouse button
            self.PopupMenu(self.contextMenu,
                           (event.x, self.parent.Size[1] - event.y))
        
        self.Bind(wx.EVT_MOUSE_CAPTURE_LOST, lambda x:None)
        
    #def pause_handler(self, event):
    #    self.pause = not self.pause
        
        
    def remove_handler(self, event):
        print "Remove"
        
    def settings_handler(self, event):
        PIDgetSettings(self, -1, "Graph's Settings")
    
    def update_plot(self, time, data):      
        """
        Append to the data array the most recent data along with the 
        current time
        """
        #TODO: Better way of referencing pause item
        if not self.contextMenu.IsChecked(self.menuItems[0][0]):
            self.append([(time, data)])
            
    def set_foreground(color):
        """
        Sets the color of the lines in the graph
        
        Input can be a tuple whose color will be used for all line or a
        list of tupples whose colors will be applied to the lines.
        
        If a list is given and the number of colors suppled is less
        than the number of lines the last color will be applied to the
        remaining lines.
        
        If invalid data is supplied, nothing will happen
        
        Params
        color -- A tupple or array of tupples containing the color(s)
            of the line(s). Color tupple is (R,G,B) out of 255
            
        Returns
        A boolean indicating if the operation was successful
        
        """
        
        retVal = True
        
        if isinstance(color, (list)):
            for aColor in color:
                if not (isinstance(color, (tupple)) and len(color) == 3):
                    retVal = False
                    break
            if len(color) < len(self.pointList):
                lastColor = color[len(color) - 1]
                for remaining in range(len(self.pointList) - len(color)):
                    color.append(lastColor)    
            self.colorList = color
        elif isinstance(color, (tupple)) and len(color) == 3:
            self.colorList = []
            for i in range(len(self.pointList)):
                self.colorList.append(color)
        else:
            retVal = False
        return retVal


class DataPanelContainer(wx.Panel):
    """
    A container for live plot panels
    
    Params
    parent -- Window displaying this panel
    minSize -- Minimum panel size tupple (width, height)
                Defaults to (380, 307) which allows all text to display
    """
    
    def __init__(self, parent, minSize = (380, 307),**kwargs):
        wx.Panel.__init__(self, parent, **kwargs)
        self._plotPanelList = []
        
        self.parent = parent
        
        self.mySizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.mySizer)
        
        self.widgetsContained = 0
        
        if minSize[0] > -1:
            self.MinPlotWidth = minSize[0]
        
        if minSize[1] > -1:
            self.MinPlotHeight = minSize[1]
        
        self.mySizer.SetMinSize((self.MinPlotWidth,
                                 self.MinPlotHeight))
        self.mySizer.SetSizeHints(parent)
        
        self.Bind(wx.EVT_RIGHT_UP, self.right_mouse_release)
        
    def right_mouse_release(self, event):
        print "Dude"
        event.Skip()

    def update_all_widgets(self):
        """
        Updates all of the widget's data
        
        The time updated on all widgets is the time that this function
        is called, not the time that the individual check was made. It
        is assumed that the difference would be negligible
        """
        time = self.parent.get_time()
        for aWidget in self._plotPanelList:
            aWidget.update_plot(time, self.parent.get_pid(aWidget.pid))
            
        
    def add_panel(self, pid, type):
        """
        Adds a new panel to the list
        
        Params:
        pid -- PID to display
        type -- Type of panel. Valid types are:
            PANEL_LINE_GRAPH -- Line graph
            PANEL_BAR_GRAPH -- Bar graph
            PANEL_GAUGE -- Gauge
            
        Returns:
        Index of the added panel
        """
        index = -1
        
        if type == PANEL_LINE_GRAPH:
            thePanel = PIDLivePlot(self, pid)
        else:
            return False

        for i in range(len(self._plotPanelList)):
            if self._plotPanelList[i] is None:
                index = i
                break
        if index == -1:
            index = len(self._plotPanelList) 
        self._plotPanelList.insert(index, thePanel)

        self.widgetsContained = self.widgetsContained + 1
        
        self.mySizer.Add(self._plotPanelList[index], 1, wx.EXPAND)
        self.mySizer.SetItemMinSize(index, (self.MinPlotWidth,
                                            self.MinPlotHeight))
        
        self.mySizer.SetMinSize((self.MinPlotWidth * self.widgetsContained,
                                 self.MinPlotHeight))
        self.mySizer.SetSizeHints(self.parent)
        return index
    
    #def new_panel(self, parent, point_list, clr_list, **kwargs):
    #    tempPanel = LivePlotPanel(parent, point_list, clr_list, **kwargs)
    #    return self.add_panel(tempPanel)
    
    #def set_panel(self, index, inPanel):
    #    """Seems like this dupes insert
    #    Puts the inPanel at the given index"""
    #    retval = False
    #    
    #    if index < len(self.plotPanelList):
    #        self._plotPanelList[index] = inPanel
    #        retval = True
    #        
    #    return retval
    
    def get_panel(self, index):
        """
        Returns the panel at the given index
        """
        
        if index < len(self._plotPanelList) and \
            self._plotPanelList[index] is not None:
            return self._plotPanelList[index]
        else:
            raise IndexError
        
    def set_min_plot_size(self, size):
        """
        Sets the minimum size of a plot to the values in the given
        "size" tupple
        """
        if size[0] > -1:
            self.MinPlotWidth = size[0]
        
        if size[1] > -1:
            self.MinPlotHeight = size[1]